'use strict';

// ready
$(document).ready(function () {

    //.search--js
    $('.search--js').click(function () {
        $(this).addClass('active');
        return false;
    });
    $('body').click(function () {
        $('.search--js').removeClass('active');
        return false;
    });
    //.search--js

    //.search--js
    $('.false--js').click(function () {
        return false;
    });
    //.search--js

    //button input
    $('.inc').click(function () {
        var $t = $(this).parent().parent().find('input');
        var step = $(this).parent().parent().find("input").attr('data-step');
        var count = parseInt($t.val()) - parseFloat(step);
        count = count < 1 ? 0 : count;
        if ($t.val() <= 1) {
            $t.addClass('zero');
        } else {
            $t.removeClass('zero');
        }
        $t.val(count);
        return false;
    });
    $('.dec').click(function () {
        var $t = $(this).parent().parent().find('input');
        var step = $(this).parent().parent().find("input").attr('data-step');
        $($t).css('display', 'inline-block');
        $t.val(parseFloat($t.val()) + parseFloat(step));
        $t.change();
        var val = $(this).parent().parent().find("input").attr('data-val');
        $(this).parent().parent().find(".product-quantity__val").text(val);
        if ($t.val() < 1) {
            $t.addClass('zero');
        } else {
            $t.removeClass('zero');
        }
        return false;
    });

    //available
    var avail = $('.available').attr('data-avail');
    var avail1 = $('.available-1');
    var avail2 = $('.available-2');
    var avail3 = $('.available-3');
    var avail4 = $('.available-4');
    var avail5 = $('.available-5');
    $('.available span').each(function () {
        if (avail < 20) {
            avail1.addClass('active');
        } else if (avail < 40) {
            avail1.addClass('active');
            avail2.addClass('active');
        } else if (avail < 60) {
            avail1.addClass('active');
            avail2.addClass('active');
            avail3.addClass('active');
        } else if (avail < 80) {
            avail1.addClass('active');
            avail2.addClass('active');
            avail3.addClass('active');
            avail4.addClass('active');
        } else {
            avail1.addClass('active');
            avail2.addClass('active');
            avail3.addClass('active');
            avail4.addClass('active');
            avail5.addClass('active');
        }
    });
    //available

    //.close--js
    $('.close--js').click(function () {
        $(this).parent().parent().remove();
        return false;
    });
    //.close--js

    // mask phone {maskedinput}
    $("[name=phone]").mask("+7 (999) 999-9999");
    // mask phone

    // slider {slick-carousel}
    // $('.slider').slick({});
    $('.slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        //infinite: false,
        centerMode: false,
        arrows: true
    });
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        // arrows: true,
        infinite: false,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        arrows: false,
        // centerMode: true,
        focusOnSelect: true
    });
    $('.slider-for1').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        infinite: false,
        asNavFor: '.slider-nav1'
    });
    $('.slider-nav1').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for1',
        arrows: false,
        // centerMode: true,
        focusOnSelect: true
    });
    $('.card-slider, .card-slider3').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        focusOnSelect: true
    });
    $('.card-slider2').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        // arrows: false,
        // centerMode: true,
        focusOnSelect: true
    });
    // slider

    // select {select2}
    $('select').select2({
        minimumResultsForSearch: Infinity
    });
    // select

    // popup {magnific-popup}
    $('.image-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1]
        },
        zoom: {
            enabled: true,
            duration: 300,
            opener: function opener(element) {
                return element.parent().find('img');
            }
        }
    });
    $('.popup-modal').magnificPopup({
        // type: 'inline',
        // preloader: false,
        // focus: '#username',
        // closeBtnInside: false
        // modal: true
    });
    // $(document).on('click', '.popup-modal-dismiss', function (e) {
    //     e.preventDefault();
    //     $.magnificPopup.close();
    // });
    // popup

    //fixed header
    $(function () {
        $(window).scroll(function () {
            var top = $(document).scrollTop();
            if (top > 220) $('.page-header__fixed').addClass('active');else $('.page-header__fixed').removeClass('active');
        });
    });
    //fixed header

    $('.popover').webuiPopover({ trigger: 'hover' });

    // range slider
    if ($("#rangeSlider").length) {
        var range = document.getElementById('rangeSlider');
        var start = document.getElementById('start');
        var end = document.getElementById('end');
        noUiSlider.create(range, {
            start: [500, 20000],
            connect: true,
            step: 10,
            range: {
                'min': 0,
                'max': 30000
            }
        });
        range.noUiSlider.on('update', function (values, handle) {
            var value = values[handle];
            if (handle) {
                end.value = Math.round(value);
            } else {
                start.value = Math.round(value);
            }
        });
        end.addEventListener('change', function () {
            range.noUiSlider.set([this.value, null]);
        });
        start.addEventListener('change', function () {
            range.noUiSlider.set([null, this.value]);
        });
    }
    // range slider

    //tabs
    $('ul.tabs__caption').on('click', 'li:not(.active)', function () {
        setInterval(function () {
            $(".card-slider3").slick("setPosition");
        }, 1);
        $(this).addClass('active').siblings().removeClass('active').closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
    });
    //tabs
});
// ready

// load
$(document).load(function () {});
// load

// scroll
$(window).scroll(function () {});
// scroll

// mobile sctipts
var screen_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
if (screen_width <= 767) {}
// mobile sctipts
//# sourceMappingURL=main.js.map